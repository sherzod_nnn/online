import HomeSection from "./sections/HomeSection";

function App() {
  return (
    <div className="app">
      <HomeSection/>
    </div>
  );
}

export default App;
