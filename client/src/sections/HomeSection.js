import React, {Component} from 'react';
import HeaderSection from "./HeaderSection";
import BodySection1 from "./BodySection1";
import BodySection2 from "./BodySection2";
import BodySection3 from "./BodySection3";
import FooterSection from "./FooterSection";

class HomeSection extends Component {
    render() {
        return (
            <div className="homeSection">
                <HeaderSection/>
                <BodySection1/>
                <BodySection2/>
                <BodySection3/>
                <FooterSection/>
            </div>
        );
    }
}

export default HomeSection;