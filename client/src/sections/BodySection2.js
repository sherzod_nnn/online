import React, {Component} from 'react';

class BodySection2 extends Component {
    render() {
        return (
            <div className="bodySection2">
                <h1>This is body 2 section</h1>
            </div>
        );
    }
}

export default BodySection2;