import React, {Component} from 'react';

class BodySection1 extends Component {
    render() {
        return (
            <div className="bodySection1">
                <h1>This is body 1 section</h1>
            </div>
        );
    }
}

export default BodySection1;