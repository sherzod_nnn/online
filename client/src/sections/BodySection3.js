import React, {Component} from 'react';

class BodySection3 extends Component {
    render() {
        return (
            <div className="bodySection3">
                <h1>This is body 3 section</h1>
            </div>
        );
    }
}

export default BodySection3;